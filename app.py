"""vismatrix 2.0
"""
import io
import timeit
import traceback
import time
import os
import logging
import random
import string
import csv
import json
import glob
import base64
import threading
from datetime import date
from datetime import datetime

import numpy as np
import requests
import cv2
from flask import Flask, request, jsonify, render_template
from flask import Response, send_from_directory, send_file
from flask_restful import Resource, Api
from flask_cors import CORS

from PIL import Image, ImageDraw, ImageFont

from src import cusutil
from src import rcode

# code repository sub-package imports
from src import rlogger
from src import rcode

# from src.routers import routers


sem = threading.Semaphore()
FONT_PATH = "./static/fonts/TimesNewRoman400.ttf"
NOT_FOUND_PATH = "./static/images/404.jpg"

# create app
app = Flask(__name__, static_url_path="")
CORS(app)
api = Api(app)

# load config
with open("config/app.json") as jfile:
    config = json.load(jfile)

APP_IP = config["APP_IP"]
APP_PORT = config["APP_PORT"]
LOG_PATH = config["LOG_PATH"]
RETURN_FORMAT = config["RETURN_FORMAT"]
DATA_FOLDER = config["DATA_FOLDER"]

# create folder structure
if not os.path.exists(LOG_PATH):
    os.mkdir(LOG_PATH)

# create logger
log_formatter = logging.Formatter("%(asctime)s %(levelname)s" " %(funcName)s(%(lineno)d) %(message)s")
log_handler = rlogger.BiggerRotatingFileHandler(
    "ali",
    LOG_PATH,
    mode="a",
    maxBytes=2 * 1024 * 1024,
    backupCount=200,
    encoding=None,
    delay=0,
)
log_handler.setFormatter(log_formatter)
log_handler.setLevel(logging.ERROR)

logger = logging.getLogger("")
logger.setLevel(logging.ERROR)
logger.addHandler(log_handler)

logger.info("INIT LOGGER SUCCESSED")

print("service ready")

# define routes and fucntion here
def load_label(fpath):
    objs = []
    x1, y1, x2, y2, cls_id, cls_name, conf = ["" for i in range(7)]
    with open(fpath, "r") as f:
        content = json.loads(f.read())

    for obj in content:
        x1 = obj["x"]
        y1 = obj["y"]
        x2 = obj["x"] + obj["w"]
        y2 = obj["y"] + obj["h"]
        cls_id = obj["label"]
        objs.append([x1, y1, x2, y2, cls_id, cls_name, conf])

    return objs


@app.route("/")
def index():
    data = {"num_page": 0, "pagefile": []}
    return render_template("index.html", data=data)


@app.route("/thumbnailimg")
def thumbnailimg():
    pagefile = []

    # get parameter
    index = int(request.args.get("index"))
    imgpath = request.args.get("imgpath")
    labelpath = request.args.get("labelpath")
    thickness = request.args.get("thickness")
    font_size = request.args.get("font_size")

    if index == None:
        index = 0
    if thickness is None or thickness == "":
        thickness = 3
    if font_size is None or font_size == "":
        font_size = 20

    IMG_PER_INDEX = 50

    imgpath = os.path.join(DATA_FOLDER, imgpath)
    labelpath = os.path.join(DATA_FOLDER, labelpath)
    print("imgpath", imgpath)
    print("labelpath", labelpath)

    pagefile = []

    for dirpath, dirname, fname in os.walk(imgpath):
        filelist = fname
    # filelist = os.listdir(imgpath)

    # calculate index
    first_index = index * IMG_PER_INDEX
    if len(filelist) - 1 > index + IMG_PER_INDEX:
        last_index = index * IMG_PER_INDEX + IMG_PER_INDEX
        page_filelist = filelist[first_index:last_index]
    else:
        last_index = len(filelist)
        page_filelist = filelist[first_index:last_index]

    # get file list
    for fname in page_filelist:
        fname_ext = "".join(fname.split(".")[:-1])
        flabel_pattern = os.path.join(labelpath, fname_ext)
        flabel_path = glob.glob("%s.*" % flabel_pattern)

        pagefile.append(
            {
                "imgpath": imgpath,
                "labelpath": labelpath,
                "flabel_path": flabel_path,
                "findex": filelist.index(fname),
                "fname": fname,
                "fname_ext": fname_ext,
                "thickness": thickness,
                "font_size": font_size,
                "boxes": "",
            }
        )

    num_page = int(len(filelist) / IMG_PER_INDEX) + 1
    data = {"num_page": num_page, "pagefile": pagefile}

    return render_template("index.html", data=data)


@app.route("/singleimg")
def singleimg():
    fname = request.args.get("fname")
    imgpath = request.args.get("imgpath")
    labelpath = request.args.get("labelpath")
    flabel_path = request.args.get("flabel_path")
    thickness = int(request.args.get("thickness"))
    font_size = int(request.args.get("font_size"))
    findex = int(request.args.get("findex"))

    for dirpath, dirname, fname1 in os.walk(imgpath):
        filelist = fname1

    fname = filelist[findex]
    fname_ext = "".join(fname.split(".")[:-1])
    flabel_pattern = os.path.join(labelpath, fname_ext)
    flabel_path = glob.glob("%s.*" % flabel_pattern)

    data = {
        "imgpath": imgpath,
        "labelpath": labelpath,
        "flabel_path": flabel_path,
        "fname": fname,
        "findex": findex,
        "thickness": thickness,
        "font_size": font_size,
    }

    return render_template("single.html", data=data)


@app.route("/get_img")
def get_img():
    imgpath = request.args.get("imgpath")
    fname = request.args.get("fname")
    fpath = os.path.join(imgpath, fname)
    flabel_path = request.args.get("flabel_path")
    thickness = int(request.args.get("thickness"))
    font_size = int(request.args.get("font_size"))

    if thickness is None or thickness == "":
        thickness = 3
    if font_size is None or font_size == "":
        font_size = 20

    if os.path.exists(fpath):
        img = cv2.imread(fpath)
    else:
        img = cv2.imread(NOT_FOUND_PATH)

    colors = {}
    font_style = ImageFont.truetype(FONT_PATH, font_size)

    objs = load_label(flabel_path)

    for obj in objs:
        x1, y1, x2, y2, cls_id, cls_name, conf = obj

        if not cls_id in colors.keys():
            colors = cusutil.gen_list_color(colors, cls_id)

        img = cv2.rectangle(img, (x1, y1), (x2, y2), colors[cls_id], thickness)

        #            cv2.putText(img, str(cls), (x1,y1-10), font, 1, colors[cls], 2, cv2.LINE_AA)
        im_pil = Image.fromarray(img)
        draw = ImageDraw.Draw(im_pil)
        draw.text((x1, y1 - 25), str(cls_id), font=font_style, fill=tuple(colors[cls_id]))
        img = np.asarray(im_pil)

    y, x, _ = img.shape
    new_width = 300
    new_height = int(new_width * y / x)
    img = cv2.resize(img, (new_width, new_height))
    ret, jpeg = cv2.imencode(".jpg", img)

    return Response(
        (b"--frame\r\n" b"Content-Type: image/jpeg\r\n\r\n" + jpeg.tostring() + b"\r\n\r\n"),
        mimetype="multipart/x-mixed-replace; boundary=frame",
    )


@app.route("/get_single_img")
def get_single_img():
    imgpath = request.args.get("imgpath")
    fname = request.args.get("fname")
    fpath = os.path.join(imgpath, fname)
    flabel_path = request.args.get("flabel_path")
    thickness = int(request.args.get("thickness"))
    font_size = int(request.args.get("font_size"))

    if thickness is None or thickness == "":
        thickness = 3
    if font_size is None or font_size == "":
        font_size = 20

    if os.path.exists(fpath):
        img = cv2.imread(fpath)
    else:
        img = cv2.imread(NOT_FOUND_PATH)

    colors = {}
    font_style = ImageFont.truetype(FONT_PATH, font_size)

    objs = load_label(flabel_path)

    for obj in objs:
        x1, y1, x2, y2, cls_id, cls_name, conf = obj

        if not cls_id in colors.keys():
            colors = cusutil.gen_list_color(colors, cls_id)

        img = cv2.rectangle(img, (x1, y1), (x2, y2), colors[cls_id], thickness)

        #            cv2.putText(img, str(cls), (x1,y1-10), font, 1, colors[cls], 2, cv2.LINE_AA)
        im_pil = Image.fromarray(img)
        draw = ImageDraw.Draw(im_pil)
        draw.text((x1, y1 - 25), str(cls_id), font=font_style, fill=tuple(colors[cls_id]))
        img = np.asarray(im_pil)

    y, x, _ = img.shape
    new_width = 1200
    new_height = int(new_width * y / x)
    img = cv2.resize(img, (new_width, new_height))
    ret, jpeg = cv2.imencode(".jpg", img)

    # send_file(io.BytesIO(jpeg), download_name=fname, mimetype="image/jpeg")
    return Response(
        (b"--frame\r\n" b"Content-Type: image/jpeg\r\n\r\n" + jpeg.tostring() + b"\r\n\r\n"),
        mimetype="multipart/x-mixed-replace; boundary=frame",
    )


if __name__ == "__main__":
    app.run(host=APP_IP, port=APP_PORT, debug=True)
